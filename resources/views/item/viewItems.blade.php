<!DOCTYPE html>
<html>
	<head>
		<title>All Items</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
	</head>
	<body>
		<div class="grid">
			@foreach($items as $item)
				<div class="grid-item">
					<a href="/items/{{ $item->id }}"><h1>{{ $item->id }}</h1></a>
					<h3>{{ $item->name }}</h3>

					<form method="post" action="/items/{{$item->id}}">
						@method('DELETE')
						@csrf
						<input class="button" type="submit" value="delete">
					</form>
					<a href="/items/{{$item->id}}/edit" class="button">update</a>

				</div>
			@endforeach
		</div>
		<a class="button" href="/items/create" style="margin: 0 auto;width: 20%;text-align: center;">Add</a>
	</body>
</html>