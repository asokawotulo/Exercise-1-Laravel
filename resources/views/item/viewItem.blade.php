<!DOCTYPE html>
<html>
	<head>
		<title>Item Details | {{title_case($item->name)}}</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
	</head>
	<body>
		<div class="view-item">
			<h1>{{ $item->id }}</h1>
			<h3>{{ title_case($item->name) }}</h3>
			<p>{{ title_case($item->price) }}</p>
			<p>{{ title_case($item->category->name) }}</p>
			<p>{{ title_case($item->created_at) }}</p>
			<p>{{ title_case($item->updated_at) }}</p>
		</div>
	</body>
</html>