<!DOCTYPE html>
<html>
	<head>
		<title>Add Item</title>
	</head>
	<body>
		<form method="post" action="/items">
			<input
				value="{{ old('name') }}" type="text"
				name="name" placeholder="Insert Name"
			>
			<input
				value="{{ old('price') }}" type="number"
				name="price" placeholder="Insert Price"
				step="0.01" min="0" max="9999.99"
			>
			<select name="category_id">
				<option selected disabled>-Choose a Category-</option>
				@foreach($categories as $category)
					<option value="{{ $category->id }}"
						@if(old('category_id')==$category->id)
							selected
						@endif
						>{{ $category->name }}</option>
				@endforeach
			</select>
			@csrf
			<input type="Submit" name="Submit">
		</form>
		@if(count($errors)>0)
			@foreach($errors->all() as $error)
				{{ ($error) }}
				<br>
			@endforeach
		@endif
	</body>
</html>