<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
	</head>
	<body>
		<form method="post" action="/items/{{$id}}">
			<input type="text" name="name" placeholder="Insert Name">
			<input type="number" name="price" placeholder="Insert Price" step="0.01" min="0" max="9999.99">
			<select name="category_id">
				<option selected disabled>--Choose a Category--</option>
				@foreach($categories as $category)
					<option value="{{ $category->id }}">{{ $category->name }}</option>
				@endforeach
			</select>
			@method('PUT')
			@csrf
			<input type="Submit" name="Submit">
		</form>
		@if(count($errors)>0)
			@foreach($errors->all() as $error)
				{{ ($error) }}
				<br>
			@endforeach
		@endif
	</body>
</html>