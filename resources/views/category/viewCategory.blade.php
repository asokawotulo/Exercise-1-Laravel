<!DOCTYPE html>
<html>
	<head>
		<title>Category Details | {{title_case($category->name)}}</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
	</head>
	<body>
		<div class="view-item">
			<h1>{{ $category->id }}</h1>
			<h3>{{ title_case($category->name) }}</h3>
			<p>{{ title_case($category->created_at) }}</p>
			<p>{{ title_case($category->updated_at) }}</p>
			{{-- <form method="post" action="/category/{{$category->id}}">
				<input name="_method" type="hidden" value="DELETE">
				@csrf
				<input class="button" type="submit" value="delete">
			</form>
			<a href="/category/{{$category->id}}/edit" class="button">update</a> --}}
		</div>
		<a href="/category/create" style="margin: 0 auto;display: block;width: 30%;text-align: center;">Add</a>
	</body>
</html>