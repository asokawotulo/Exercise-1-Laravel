<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
	</head>
	<body>
		<form method="post" action="/category/{{$category}}">
			<input type="text" name="name" placeholder="Insert Name">
			@method('PUT')
			@csrf
			<input type="Submit" name="Submit">
		</form>
	</body>
</html>