<!DOCTYPE html>
<html>
	<head>
		<title>All Categories</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
	</head>
	<body>
		<div class="grid">
			@foreach($categories as $category)
				<div class="grid-item">
					<a href="/category/{{ $category->id }}"><h1>{{ $category->id }}</h1></a>
					<h3>{{ $category->name }}</h3>

					<form method="post" action="/category/{{$category->id}}">
						@method('DELETE')
						@csrf
						<input class="button" type="submit" value="delete">
					</form>
					<a href="/category/{{$category->id}}/edit" class="button">update</a>

				</div>
			@endforeach
		</div>
		<a class="button" href="/category/create" style="margin: 0 auto;width: 20%;text-align: center;">Add</a>
	</body>
</html>