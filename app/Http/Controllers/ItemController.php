<?php

namespace App\Http\Controllers;

use Validator;
use App\Items;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Items::all();
        return view('item.viewItems', ['items'=>$items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = \App\Category::all();
        return view('item.addItem', ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'price'=>'required|numeric|between:0.01,9999.99',
            'category_id'=>'required|string|exists:categories,id',
        ]);
        if($validator->fails()){
            return redirect('/items/create')->withErrors($validator)->withInput();
        } else if(Items::Create($request->all())){
            return redirect('/items');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Items::find($id);
        return view('item.viewItem', ['item'=>$item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = \App\Category::all();
        return view('item.updateItem', ['categories'=>$categories, 'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $inputs = $request->input();
        $inputs = array_filter($inputs, function ($val){return !is_null($val);});
        $item = Items::find($id);

        $validator = Validator::make($request->all(), [
            'name'=>'nullable|string',
            'price'=>'nullable|numeric|between:0.01,9999.99',
            'category_id'=>'nullable|string|exists:categories,id',
        ]);
        
        if($validator->fails()){
            return redirect('/items/'.$id.'/edit')->withErrors($validator)->withInput();
        } else if($item->fill($inputs)->save()){
            return redirect('/items');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = \App\Items::find($id);
        if($item->delete($id)){
            return redirect('items');
        }
    }
}
