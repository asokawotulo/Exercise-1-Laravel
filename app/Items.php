<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'items';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'price', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
